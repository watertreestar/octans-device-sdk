module gitee.com/watertreestar/octans-device-sdk

go 1.19

require (
	github.com/jasonlvhit/gocron v0.0.1
	github.com/loozhengyuan/hikvision-sdk v0.0.0-20200816120645-540af54696c2
	github.com/lunixbochs/struc v0.0.0-20200707160740-784aaebc1d40
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/quocson95/go-onvif v1.0.6
	github.com/simonvetter/modbus v1.6.0
)

require (
	github.com/clbanning/mxj v1.8.4 // indirect
	github.com/goburrow/serial v0.1.0 // indirect
	github.com/golang/glog v1.1.2 // indirect
	github.com/google/uuid v1.3.1 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
)
