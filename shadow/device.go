package shadow

import "time"

type Device struct {
	Name      string
	ttl       time.Duration
	online    bool
	updatedAt time.Time
}

func (d *Device) setTTL(t time.Duration) {
	d.setTTL(t)
}

func NewDevice(name string, ttl time.Duration) Device {
	return Device{
		Name:   name,
		online: false,
		ttl:    ttl,
	}
}
