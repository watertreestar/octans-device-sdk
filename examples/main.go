package main

import (
	"fmt"
	device "gitee.com/watertreestar/octans-device-sdk"
	"gitee.com/watertreestar/octans-device-sdk/env"
	"gitee.com/watertreestar/octans-device-sdk/log"
	"time"
)

func main() {
	err := device.Run(device.ConnectorConfiguration{
		PanelPort:   5012,
		VehiclePort: 9000,
		Logger:      &log.DefaultLogger{},
		StatusChangeCallback: func(name string, online bool) {
			fmt.Printf("%s 设备在线状态：%v \n", name, online)
		},
	})

	go func() {
		for {
			onlineDevice := device.OnlineDevice()
			fmt.Printf("当前在线设备sn： %v \n", onlineDevice)
			time.Sleep(time.Second * 10)
		}
	}()

	if err != nil {
		panic(err)
	}

	//conn, err := modbus.NewClient(&modbus.ClientConfiguration{
	//	URL:     fmt.Sprintf("tcp://%s:%d", "192.168.236.110", 10125),
	//	Timeout: time.Second * 5,
	//})
	//if err != nil {
	//	return
	//}
	//err = conn.Open()
	//if err != nil {
	//	return
	//}
	//
	//for true {
	//	values, err := conn.ReadRegisters(0, 2, modbus.HOLDING_REGISTER)
	//	if err != nil {
	//		fmt.Printf("%v\n", err)
	//		continue
	//	}
	//	fmt.Printf("%v", values)
	//	time.Sleep(time.Second * 4)
	//}

	go testEnv()

	//go testIpc()

	//go testVehicle()

	//go testPanel()
	var stop = make(chan bool, 1)
	<-stop
}

func testIpc() {
	// 通过onvif发现设备
	// 通过onvif获取拉流地址
	// 添加onvif设备，为了监控状态
	devices, err := device.Ipc.Discovery()
	if err != nil {
		fmt.Printf("%v ", err)
	}
	fmt.Printf("发现的onvif设备列表：%s ", devices)
	for _, d := range devices {
		device.Ipc.AddDevice(d.ID, d.XAddr, "admin", "ipc@2023")
		url, err := device.Ipc.GetStreamUrl("http://192.168.236.104/onvif/device_service", "admin", "ipc@2023")
		if err != nil {
			fmt.Printf("%v ", err)
			continue
		}
		fmt.Println(url)
	}
}

func testEnv() {
	// 添加删除其它检测设备，并读取气体检测值
	valChan := device.Env.GetValChan()
	err := device.Env.AddDevice("123", "192.168.236.99", 10125, env.GasType, 4)
	if err != nil {
		fmt.Printf("%v", err)
	}
	err = device.Env.AddDevice("122", "192.168.236.99", 10123, env.TempType, 2)
	if err != nil {
		fmt.Printf("%v \n", err)
	}
	go func() {
		for v := range valChan {
			fmt.Printf("gas val: %v \n", v)
		}
	}()

	time.Sleep(time.Second * 60)
	err = device.Env.RemoveDevice("123")
	if err != nil {
		fmt.Printf("删除气体检测设备错误%s ", err)
	}
}

func testVehicle() {
	ve := device.Vehicle
	sdevices, err := ve.Discovery()
	if err != nil {
		fmt.Printf("discovery vehicle err: %v ", err)
		return
	}
	fmt.Printf("车行门禁控制卡数量: %d ", len(sdevices))
	for _, d := range sdevices {
		fmt.Printf("发现设备：%v ", d)
		if err := ve.AddDevice("ve-123", d.SerialNum, d.IP); err != nil {
			fmt.Println(err)
			continue
		}
	}
	// 清空所有权限
	err = ve.ClearPrivilege("ve-123")
	if err != nil {
		fmt.Println(err)
	}
	// 添加卡号权限
	err = ve.AddPrivilege("ve-123", 3671303)
	if err != nil {
		fmt.Printf("添加权限失败 %v", err)
	}

	go func() {
		// 20s后删除该卡号的权限
		<-time.NewTimer(time.Second * 20).C
		err := ve.RemovePrivilege("ve-123", 3671303)
		if err != nil {
			fmt.Printf("删除权限失败 %v", err)
		}
	}()
	// 读取刷卡事件
	eventchan := ve.GetSwipeEvent()
	for data := range eventchan {
		fmt.Println(data)
	}
}

// 没有经过设备测试，需要验证
func testPanel() {
	panel := device.Panel
	// 添加设备
	// 第一个参数为唯一编号
	// 第二个参数为人脸识别机的序列号
	err := panel.AddDevice("p-1222", "AB4F4C9B-0EB55155")
	if err != nil {
		fmt.Printf("add panel device error, %v", err)
		return
	}

	// 读取刷卡结果
	resultChan := panel.GetResultChan()
	go func() {
		for result := range resultChan {
			fmt.Printf("收到识别数据： %v \n\n", result)
		}
	}()

	time.Sleep(time.Second * 10)

	// 人脸数据不可超过512KB
	imageData := ""
	// 发送指令，并阻塞等待返回结果， 结果中具体的内容参考协议文档
	// 参数说明：
	// 第一个参数为sn: 人脸识别相机的序列号
	// 第二个参数为协议内容，字段参考文档，不用写index和version字段
	// 第三个参数为等待返回的超时时间
	fmt.Println("创建人脸")
	result, err := panel.Send("p-1222", map[string]interface{}{
		"cmd":       "create_face", //创建人员指令
		"per_id":    "100000",      //人员id：不超过32位
		"per_name":  "tony",        //姓名：不超过10字节 utf8编码
		"img_data":  imageData,     //base64
		"per_type":  0,             //0：普通人员； 1：白名单人员；2：黑名单人员(可选)
		"usr_type":  0,             //权限类型，取值 0 - 5，默认 0(可选)
		"auth_type": 0,             //拓展权限，取值1-50，默认0（可选）
	}, time.Second*50)
	if err != nil {
		fmt.Printf("send data error, %v \n", err)
		return
	}
	fmt.Printf("创建人脸返回 %v \n", result)
}
