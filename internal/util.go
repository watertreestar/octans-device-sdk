package internal

import "sync"

func Contains(key string, keys []string) bool {
	for _, k := range keys {
		if k == key {
			return true
		}
	}
	return false
}

func BCD(a uint8) uint8 {
	return a + (a/10)*6
}

type IDGenerator struct {
	mu    sync.Mutex
	value int32
}

func NewIDGenerator(startValue int32) *IDGenerator {
	return &IDGenerator{value: startValue}
}

func (g *IDGenerator) Generate() int32 {
	g.mu.Lock()
	defer g.mu.Unlock()
	id := g.value
	g.value++
	return id
}
