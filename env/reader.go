package env

import (
	"errors"
	"github.com/simonvetter/modbus"
	"math"
)

func init() {
	initReaders()
}

type reader = func(client *modbus.ModbusClient, d *modbusDevice) ([]float64, error)

var readerRegister = make(map[SensorType]reader, 0)

func initReaders() {
	// gas sensor reader
	readerRegister[GasType] = func(client *modbus.ModbusClient, d *modbusDevice) ([]float64, error) {
		regvals, err := d.conn.ReadRegisters(0, d.quantity, modbus.HOLDING_REGISTER)
		if err != nil {
			return nil, err
		}
		accuracy, err := d.conn.ReadRegisters(0x0050, d.quantity, modbus.HOLDING_REGISTER)
		if err != nil {
			return nil, err
		}
		if len(regvals) != len(accuracy) {
			return nil, errors.New("length of regval is not equal to length of accuracy")
		}
		var result []float64
		for i := 0; i < len(regvals); i++ {
			floatValue := float64(regvals[i])
			factor := math.Pow(10, float64(accuracy[i]))
			val := floatValue / factor
			result = append(result, val)
		}
		return result, nil
	}

	readerRegister[TempType] = func(client *modbus.ModbusClient, d *modbusDevice) ([]float64, error) {
		regvals, err := d.conn.ReadRegisters(0, d.quantity, modbus.HOLDING_REGISTER)
		if err != nil {
			return nil, err
		}
		if err != nil {
			return nil, err
		}
		var result []float64
		for _, value := range regvals {
			if value&0x80 == 0 {
				result = append(result, float64(value)/10)
			} else {
				v := ^(value) + 1
				result = append(result, float64(-v)/10)
			}
		}

		return result, nil
	}
}
