package env

import (
	"fmt"
	"gitee.com/watertreestar/octans-device-sdk/log"
	"gitee.com/watertreestar/octans-device-sdk/shadow"
	"github.com/jasonlvhit/gocron"
	"github.com/simonvetter/modbus"
	"sync"
	"time"
)

type SensorType = uint8

const (
	GasType  SensorType = 0x00
	TempType SensorType = 0x10
)

type SensorVal struct {
	SN   string    `json:"sn"`
	IP   string    `json:"ip"`
	Port int       `json:"port"`
	Val  []float64 `json:"values"`
}

type modbusDevice struct {
	sn       string
	conn     *modbus.ModbusClient
	ip       string
	port     int
	quantity uint16 // register size to read
	t        SensorType
}

type Connector struct {
	devices []*modbusDevice
	// channel for pulled value
	valChan chan SensorVal
	s       *gocron.Scheduler
	mutex   *sync.Mutex
	logger  log.Logger
}

func NewConnector(logger log.Logger) *Connector {
	return &Connector{
		logger: logger,
		mutex:  &sync.Mutex{},
	}
}

func (c *Connector) Initialize() error {
	// get modbusDevice pool on starting
	c.s = gocron.NewScheduler()
	c.s.Start()
	if err := c.schedule(); err != nil {
		return err
	}
	return nil
}

func (c *Connector) GetValChan() chan SensorVal {
	return c.valChan
}

func (c *Connector) Destroy() error {
	close(c.valChan)
	c.disconnect()
	return nil
}

func (c *Connector) schedule() error {
	var data = make(chan SensorVal)
	c.pollModbusVal(data)
	c.valChan = data
	return nil
}

// AddDevice 添加设备
// sn 设备唯一编号
// ip  气体传感器ip
// port 其它传感器port
// quantity 读取的寄存器数量
func (c *Connector) AddDevice(sn, ip string, port int, t SensorType, quantity uint16) error {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	if err := shadow.Of().AddDevice(sn, time.Second*30); err != nil {
		return err
	}
	c.devices = append(c.devices, &modbusDevice{
		conn:     nil,
		sn:       sn,
		ip:       ip,
		port:     port,
		quantity: quantity,
		t:        t,
	})
	return nil
}

// RemoveDevice 移除气体检测设备
func (c *Connector) RemoveDevice(sn string) error {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	if err := shadow.Of().RemoveDevice(sn); err != nil {
		return err
	}
	devices := c.devices
	var filter []*modbusDevice
	for _, d := range devices {
		t := d
		if t.sn != sn {
			filter = append(filter, t)
		} else {
			if t.conn != nil {
				_ = t.conn.Close()
			}
		}
	}
	c.devices = filter
	return nil
}

func (c *Connector) disconnect() {
	for _, c := range c.devices {
		if c.conn != nil {
			if err := c.conn.Close(); err != nil {
			}
		}
	}
	c.devices = []*modbusDevice{}
}

func (c *Connector) pollModbusVal(valChan chan<- SensorVal) {
	err := c.s.Every(10).Seconds().Do(func() {
		for _, d := range c.devices {
			if d.conn == nil {
				d.conn = connectToModbus(d.ip, d.port)
			}
			if d.conn == nil {
				continue
			}
			// read register val from holding register with a quantity
			if reader, ok := readerRegister[d.t]; ok {
				regvals, err := reader(d.conn, d)
				if err != nil {
					c.logger.Warn("failed to read environment sensor device value,reconnect on next time, "+
						"ip: %s, port: %d, err: %s", d.ip, d.port, err)
					continue
				}
				_ = shadow.Of().RefreshUpdateAt(d.sn)
				valChan <- SensorVal{
					SN:   d.sn,
					IP:   d.ip,
					Port: d.port,
					Val:  regvals,
				}
			}

		}
	})
	if err != nil {
		panic(err)
	}
}

func connectToModbus(ip string, port int) *modbus.ModbusClient {
	conn, err := modbus.NewClient(&modbus.ClientConfiguration{
		URL:     fmt.Sprintf("udp://%s:%d", ip, port),
		Timeout: time.Second * 2,
	})
	if err != nil {
		return nil
	}
	if err = conn.Open(); err != nil {
		return nil
	}
	return conn
}
