package device

import (
	"gitee.com/watertreestar/octans-device-sdk/env"
	"gitee.com/watertreestar/octans-device-sdk/ipc"
	"gitee.com/watertreestar/octans-device-sdk/log"
	"gitee.com/watertreestar/octans-device-sdk/panel"
	"gitee.com/watertreestar/octans-device-sdk/shadow"
	"gitee.com/watertreestar/octans-device-sdk/vehicle"
)

type ConnectorConfiguration struct {
	PanelPort            int // 人脸识别面板机连接端口
	VehiclePort          int // 车行门禁事件上报连接端口
	Logger               log.Logger
	StatusChangeCallback func(name string, online bool)
}

var (
	Env     *env.Connector
	Ipc     *ipc.Connector
	Panel   *panel.Connector
	Vehicle *vehicle.Connector
)

func Run(conf ConnectorConfiguration) error {
	Env = env.NewConnector(conf.Logger)
	if err := Env.Initialize(); err != nil {
		return err
	}
	Ipc = ipc.NewConnector(conf.Logger)
	if err := Ipc.Initialize(); err != nil {
		return err
	}
	Panel = panel.NewConnector(conf.PanelPort, conf.Logger)
	if err := Panel.Initialize(); err != nil {
		return err
	}
	Vehicle = vehicle.NewConnector(conf.VehiclePort, conf.Logger)
	if err := Vehicle.Initialize(); err != nil {
		return err
	}
	shadow.Of().OnlineChangeCallback(conf.StatusChangeCallback)
	return nil
}

func OnlineDevice() []string {
	return shadow.Of().OnlineDevice()
}

func GetStatus(sn string) (online bool, err error) {
	return shadow.Of().GetStatus(sn)
}
