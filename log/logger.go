package log

import (
	"fmt"
	"os"
)

type Logger interface {
	Debug(template string, args ...interface{})
	Info(template string, args ...interface{})
	Warn(template string, args ...interface{})
	Error(template string, args ...interface{})
}

type DefaultLogger struct {
}

func (l DefaultLogger) Debug(template string, args ...interface{}) {
	os.Stdout.WriteString(fmt.Sprintf("[%s]: %s \n", "Debug", formatLog(template, args...)))
}

func (l DefaultLogger) Info(template string, args ...interface{}) {
	os.Stdout.WriteString(fmt.Sprintf("[%s]: %s \n", "Info ", formatLog(template, args...)))
}

func (l DefaultLogger) Warn(template string, args ...interface{}) {
	os.Stdout.WriteString(fmt.Sprintf("[%s]: %s \n", "Warn ", formatLog(template, args...)))
}

func (l DefaultLogger) Error(template string, args ...interface{}) {
	os.Stdout.WriteString(fmt.Sprintf("[%s]: %s \n", "Error", formatLog(template, args...)))
}

func formatLog(f string, v ...interface{}) string {
	var data []any
	for i := 0; i < len(v); i++ {
		data = append(data, v[i])
	}
	return fmt.Sprintf(f, data...)
}
